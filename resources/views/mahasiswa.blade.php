@extends('layout/main')

@section('title', 'Mahasiswa')

@section('container')

<div class="container">
<div class="row">
<div class="col-10">
    <h1 class="mt-3">Daftar IPK Tertinggi </h1>

    <table class="table">
        <thead>
          <tr>
            <th scope="col">No</th>
            <th scope="col">Nama</th>
            <th scope="col">Nim</th>
            <th scope="col">Jurusan</th>
            <th scope="col">IPK</th>
          </tr>
        </thead>
        <tbody>
            @foreach ( $mahasiswa as $mhs )
          <tr>
            <th scope="row">{{ $loop->iteration}}</th>
            <td>{{ $mhs->Nama }}</td>
            <td>{{ $mhs->Nim }}</td>
            <td>{{ $mhs->Jurusan }}</td>
            <td>{{ $mhs->IPK }}</td>
          </tr>
         
          @endforeach
        </tbody>
      </table>

</div>
</div>
</div>
@endsection
   